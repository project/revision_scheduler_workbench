<?php

namespace Drupal\revision_scheduler\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller to handle Revision Schedule request.
 */
class RevisionScheduler extends ControllerBase {

  /**
   * Start scheduling revision using this method.
   *
   * @return output
   *   Containing the message whether it is successful or not.
   */
  public function revision() {
    $state_transition = \Drupal::config('revision_scheduler.transition')->get('state_transition');
    $query = \Drupal::database()->select('node_revision__field_publish_on', 'nfp');
    $query->innerJoin('node_field_revision', 'nfr', 'nfp.revision_id = nfr.vid');
    $query->condition('nfr.moderation_state', $state_transition, '=');
    $query->condition('nfp.field_publish_on_value', REQUEST_TIME, '<=');
    $query->fields('nfr', ['vid', 'nid']);
    $query->fields('nfp', ['field_publish_on_value']);
    $query->orderBy('nfr.vid', 'DESC');
    $revisions = $query->execute()->fetchAll();
    // Storing the current execution time or REQUEST_TIME in variable.
    $min_value = REQUEST_TIME;
    // To check if there are any revisions that are to be scheduled or not.
    if (!empty($revisions)) {
      foreach ($revisions as $key => $value) {
        // Storing the result set in a temp array.
        $temp_array[$value->nid][$value->vid] = $value->field_publish_on_value;
      }
      $revision_array = RevisionScheduler::getRevisionsToPublish($temp_array);
      foreach ($revision_array as $timestamp) {
        // To check if for a particular node ID there exist more than one Vid.
        if (count($timestamp) > 1) {
          foreach ($timestamp as $key => $value) {
            // Creating array differnce between publish time and request time.
            $check_array[$key] = $min_value - $value;
          }
          $value_min = array_keys($check_array, min($check_array));
          if (is_array($value_min)) {
            $value_min = $value_min[0];
          }
          RevisionScheduler::publishNode($value_min);
          unset($check_array);
        }
        else {
          RevisionScheduler::publishNode(array_keys($timestamp));
        }
      }
      $output = drupal_set_message($this->t('The nodes have been published'));
    }
    else {
      $output = drupal_set_message($this->t('There are no nodes to publish.'), 'error');
    }
    return $output;
  }

  /**
   * Start publishing the node revision using this method.
   *
   * @param string $entity_id
   *   Entity id is the revision Id which is passed from the above method.
   */
  public function publishNode($entity_id) {
    if (is_array($entity_id)) {
      $entity_id = implode(',', $entity_id);
    }
    $entity = entity_revision_load('node', $entity_id);
    $entity->log = "Node published via scheduler.";
    $entity->setPublished(TRUE);
    $entity->set('changed', time());
    $entity->set('moderation_state', 'published');
    $entity->save();
  }

  /**
   * To get the revisions which are scheduled after the latest publish date.
   *
   * @param string $revision_ids
   *   The array with all elements which are in ready for publish.
   */
  public function getRevisionsToPublish($revision_ids) {
    foreach ($revision_ids as $nid => $vid) {
      $query = \Drupal::database()->select('node_revision__field_publish_on', 'nfp');
      $query->innerJoin('node_field_revision', 'nfr', 'nfp.revision_id = nfr.vid');
      $query->condition('nfr.moderation_state', 'published', '=');
      $query->condition('nfr.nid', $nid);
      $query->fields('nfr', ['vid']);
      $query->fields('nfp', ['field_publish_on_value']);
      $query->orderBy('nfr.vid', 'DESC');
      $revisions = $query->execute()->fetchAssoc();
      foreach ($vid as $key => $field_publish_on_value) {
        if ($revisions['field_publish_on_value'] < $field_publish_on_value) {
          $revision_array[$nid][$key] = $field_publish_on_value;
        }
      }
    }
    return $revision_array;
  }

}
