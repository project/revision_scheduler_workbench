<?php

namespace Drupal\revision_scheduler_workbench\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\revision_scheduler\Form\RevisionSchedulerAdminForm;

/**
 * Controller to handle Revision Schedule request.
 */
class RevisionSchedulerTab extends ControllerBase {

  /**
   * Start scheduling revision using this method.
   *
   * @return output
   *   Containing the message whether it is successful or not.
   */
  public function display($node) {
    $state_transition = \Drupal::config('revision_scheduler.transition')->get('state_transition');
    $header = [$this->t('Title'),
          $this->t('Scheduled Release Date'), $this->t('Current Moderation State')
          ];
    $query = db_select('node_revision__field_publish_on', 'nfp');
    $query->innerJoin('node_field_revision', 'nfr', 'nfp.revision_id = nfr.vid');
    $query->condition('nfr.moderation_state', $state_transition, '=');
    $query->condition('nfp.entity_id', $node, '=');
    $query->condition('nfp.field_publish_on_value', REQUEST_TIME, '>=');
    $query->condition('nfr.moderation_state', 'published', '!=');
    $query->fields('nfr', ['title', 'moderation_state']);
    $query->fields('nfp', ['field_publish_on_value']);
    $revisions = $query->execute()->fetchAll();
    $rows = [];
    $i = 0;
    $states = RevisionSchedulerAdminForm::getWorkbenchStateLabels();

    // To get all scheduled revisions of node and create table structure based on it.
    foreach ($revisions as $key => $value) {
      $rows[$i][] = $value->title;
      $date = gmdate("F dS,Y", $value->field_publish_on_value);
      $rows[$i][] = "Scheduled to publish on " . $date;
      $rows[$i][] = $states[$state_transition];
      $i++;
    }
    $build['node_schedule_list'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    $build['pager'] = ['#type' => 'pager'];

    return $build;
  }

}
