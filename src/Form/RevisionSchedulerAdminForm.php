<?php

namespace Drupal\revision_scheduler_workbench\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\workbench_moderation\Entity\ModerationState;

/**
 * Defines a form that configures forms module settings.
 */
class RevisionSchedulerAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'revision_scheduler.transition',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'revision_scheduler_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity_ids = RevisionSchedulerAdminForm::getWorkbenchStateLabels();
    $state_transition = \Drupal::config('revision_scheduler.transition')->get('state_transition');
    $form['state_transition'] = [
      '#title' => t('State on which scheduler should work'),
      '#type' => 'select',
      '#description' => "Select the state from which transition for scheduler should work.",
      '#options' => $entity_ids,
      '#default_value' => $state_transition,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    \Drupal::configFactory()->getEditable('revision_scheduler.transition')
      ->set('state_transition', $form_state->getValue('state_transition'))
      ->save();
  }

  /**
   * Get all the labels of workbench moderation states.
   */
  public function getWorkbenchStateLabels() {
    foreach (ModerationState::loadMultiple() as $moderation_state) {
      if ($moderation_state->id() != 'published') {
        $options[$moderation_state->id()] = $moderation_state->label();
      }
    }
    return $options;
  }

}
